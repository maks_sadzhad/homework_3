// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homework_3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_3_API Ahomework_3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
